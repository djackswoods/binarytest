/**
 * 
 * @author Daniel Jackson
 * @class  CS260
 * @assignment Lab 1
 *
 */


public class BinaryNumber implements BinaryADT {

	private String s;
	/*
	 * creates the binary number and runs the string through the checkString method
	 */
	public BinaryNumber(String contents) {
		s = new String(contents);
		if (!checkString(s) || s.length() == 0) {
			throw new IllegalArgumentException("Cannot contain letters");
		}

	}
	/*
	 * Checks the string being passed to it to make sure that the string is not null, 
	 * contains no letters, and only contains 1's and 0's, if it passes
	 * @returns true
	 * if it fails
	 * @returns false
	 */
	public static boolean checkString(String s) {
		int i = 0;
		if (s == null) {
			throw new IllegalArgumentException("string is empty");
		}
		for (i = 0; i < s.length(); i++) {
			if (!Character.isDigit(s.charAt(i))) {
				return false;
			}
		}
		i = 0;
		char[] checkString = { '2', '3', '4', '5', '6', '7', '8', '9' };
		while (i < s.length()) {
			for (char y : checkString) {
				if (s.charAt(i) == y)
					return false;
			}
			i++;
		}
		return true;
	}
	/*
	 * @overrides the toString() method of the string class(non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		return s;
	}

	
	/*
	 * this is the method that takes in no parameters, but uses the string given it
	 * and conducts the binary NOT operation on the string. ex: if x = 111
	 * the returned string will be 000(non-Javadoc)
	 * @see BinaryADT#not()
	 */
	public BinaryADT not() {
		BinaryNumber temp1 = this;
		char[] temp2 = temp1.s.toCharArray();
		String text = "";
		for (char y : temp2) {
			if (y == '1') {
				text = text + '0';
			} else {
				text = text + '1';
			}
		}
		BinaryNumber temp3 = new BinaryNumber(text);
		return temp3;
	}

	/*
	 * takes in two strings x and y, then conducts the binary AND operation on the strings
	 * it also checks to see if either of the values are null, if they are
	 * @returns null
	 * Then checks to make sure the strings are of even length, if they are not equal lengths
	 * it buffers the shorter string with 0's until they are equal. if it passes
	 * returns the string after the operation
	 * ex: x = 111 and y = 000 z = 000(non-Javadoc)
	 * @see BinaryADT#and(BinaryADT)
	 */
	public BinaryADT and(BinaryADT y) {
		if(y == null)
		{
			return null;
		}
		char[] temp = this.s.toCharArray();
		BinaryNumber temp2 = (BinaryNumber) y;
		char[] temp3 = temp2.s.toCharArray();
		String text = "";
		String concat = "";
		String b = new String(temp);
		String c = new String(temp3);
		while(b.length() != c.length())
		{
			if(b.length() < c.length())
			{
				int dif = c.length() - b.length();
				for(int i = 0; i < dif;i++)
				{
					concat += "0";
				}
				b = concat + b;
			}else if(c.length() < b.length())
			{
				int dif = b.length() - c.length();
				for(int i = 0; i < dif; i++)
				{
					concat += "0";
				}
				c = concat + c;
			}
		}
		temp = b.toCharArray();
		temp3 = c.toCharArray();
		for (int i = 0; i < temp.length; i++) {
			if (temp[i] == '1' && temp3[i] == '1') {
				text += "1";
			} else {
				text += "0";
			}
		}
		BinaryNumber temp4 = new BinaryNumber(text);
		return temp4;
	}

	/*
	 *  takes in two strings x and y, then conducts the binary OR operation on the strings
	 * it also checks to see if either of the values are null, if they are
	 * @returns null
	 * Then checks to make sure the strings are of even length, if they are not equal lengths
	 * it buffers the shorter string with 0's until they are equal. if it passes
	 * returns the string after the operation
	 * ex: x = 111 or y = 000 z = 111(non-Javadoc)
	 * @see BinaryADT#or(BinaryADT)
	 */
	public BinaryADT or(BinaryADT y) {
		if(y == null)
		{
			return null;
		}
		char[] temp = this.s.toCharArray();
		BinaryNumber temp2 = (BinaryNumber) y;
		char[] temp3 = temp2.s.toCharArray();
		String text = "";
		String concat = "";
		String b = new String(temp);
		String c = new String(temp3);
		while(b.length() != c.length())
		{
			if(b.length() < c.length())
			{
				int dif = c.length() - b.length();
				for(int i = 0; i < dif;i++)
				{
					concat += "0";
				}
				b = concat + b;
			}else if(c.length() < b.length())
			{
				int dif = b.length() - c.length();
				for(int i = 0; i < dif; i++)
				{
					concat += "0";
				}
				c = concat + c;
			}
		}
		temp = b.toCharArray();
		temp3 = c.toCharArray();
		for (int i = 0; i < temp.length; i++) {
			if (temp[i] == '0' && temp3[i] == '0') {
				text += "0";
			} else {
				text += "1";
			}
		}
		BinaryNumber temp4 = new BinaryNumber(text);
		return temp4;
		
	}

	/*
	 *  takes in two strings x and y, then conducts the binary XOR operation on the strings
	 * it also checks to see if either of the values are null, if they are
	 * @returns null
	 * Then checks to make sure the strings are of even length, if they are not equal lengths
	 * it buffers the shorter string with 0's until they are equal. if it passes
	 * returns the string after the operation
	 * ex: x = 010 XOR y = 110 z = 100(non-Javadoc)
	 * @see BinaryADT#xor(BinaryADT)
	 */
	public BinaryADT xor(BinaryADT y) {
		if(y == null)
		{
			return null;
		}
		char[] temp = this.s.toCharArray();
		BinaryNumber temp2 = (BinaryNumber) y;
		char[] temp3 = temp2.s.toCharArray();
		String text = "";
		String concat = "";
		String b = new String(temp);
		String c = new String(temp3);
		while(b.length() != c.length())
		{
			if(b.length() < c.length())
			{
				int dif = c.length() - b.length();
				for(int i = 0; i < dif;i++)
				{
					concat += "0";
				}
				b = concat + b;
			}else if(c.length() < b.length())
			{
				int dif = b.length() - c.length();
				for(int i = 0; i < dif; i++)
				{
					concat += "0";
				}
				c = concat + c;
			}
		}
		temp = b.toCharArray();
		temp3 = c.toCharArray();
		for (int i = 0; i < temp.length; i++) {
			if ((temp[i] == '1' && temp3[i] == '0') || ((temp[i] == '0' && temp3[i] == '1')) ) {
				text += "1";
			} else {
				text += "0";
			}
		}
		BinaryNumber temp4 = new BinaryNumber(text);
		return temp4;
	}

	/*
	 *  takes in two strings x and y, then conducts the string.equals() operation on the strings
	 * it also checks to see if either of the values are null, if they are
	 * @returns null
	 * Then checks to make sure the strings are of even length, if they are not equal lengths
	 * it buffers the shorter string with 0's until they are equal. if it passes
	 * returns the string after the operation
	 * ex: x = 111 and y = 000 z = false(non-Javadoc)
	 * @see BinaryADT#equals(BinaryADT)
	 */
	public boolean equals(BinaryADT y) {
		if(y == null)
		{
			return false;
		}
		char[] temp = this.s.toCharArray();
		BinaryNumber temp2 = (BinaryNumber) y;
		char[] temp3 = temp2.s.toCharArray();
	
		String concat = "";
		String b = new String(temp);
		String c = new String(temp3);
		while(b.length() != c.length())
		{
			if(b.length() < c.length())
			{
				int dif = c.length() - b.length();
				for(int i = 0; i < dif;i++)
				{
					concat += "0";
				}
				b = concat + b;
			}else if(c.length() < b.length())
			{
				int dif = b.length() - c.length();
				for(int i = 0; i < dif; i++)
				{
					concat += "0";
				}
				c = concat + c;
			}
		}
		//temp = b.toCharArray();
		//temp3 = c.toCharArray();
	
		if (b.equals(c)) {
			return true;
		} else {
			return false;
		}
	}
	public static void main

}
