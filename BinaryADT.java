
/**
 * Specification for a Binary number abstract data type.  In this definition, Binary numbers 
 * are immutable, i.e. they cannot be changed.  Therefore all these methods should not modify 
 * the original object but should create a new object that holds the desired value. 
 */
public interface BinaryADT
{
	/**
	 *	Bitwise NOT operation.  e.g. NOT(01101) is 10010
	 *	@return The Binary number that is the NOT of this Binary number.
	 */
	public BinaryADT not();
	
	/**
	 * Bitwise AND operation.  e.g. x = 101 and y = 011 then x.and(y) is 001
	 *	@param y The Binary number to use as the second operand
	 *	@return The Binary number that is the AND of this Binary number and the argument y, or 
	 * 			null if the operation cannot be completed
	 */
	public BinaryADT and( BinaryADT y );
	
	/**
	 * Bitwise OR operation.  e.g. x = 101 and y = 011 then x.or(y) is 111
	 *	@param y The Binary number to use as the second operand
	 *	@return The Binary number that is the OR of this Binary number and the argument y, or 
	 * 			null if the operation cannot be completed
	 */
	public BinaryADT or( BinaryADT y );
	
	/**
	 * Bitwise XOR operation.  e.g. x = 101 and y = 011 then x.xor(y) is 110
	 *	@param y The Binary number to use as the second operand
	 *	@return The Binary number that is the XOR of this Binary number and the argument y, or 
	 * 			null if the operation cannot be completed
	 */
	public BinaryADT xor( BinaryADT y );
	
	/**
	 * Determine if this Binary number is equal to, i.e. the same as, the given Binary number argument.
	 *	@param y The Binary number to use as the second operand
	 *	@return True if the two Binary numbers represent the exact same sequence of binary digits, False otherwise.
	 *		Leading zeros do matter.
	 */
	public boolean equals( BinaryADT y );

}
