import junit.framework.*;


/**
 * JUnit test class for the BinaryADT implementation: BinaryNumber
 */
public class BinaryNumberTest extends TestCase 
{	
	public BinaryNumberTest( String str )
	{
		super( str );
	}	
	
		//*********************  Test methods ************************
	
	public void testConstructor1()
	{
		try
		{
			Class<?> c = Class.forName("BinaryNumber");
			Object o = c.getConstructor();
			fail();
		}
		catch( Exception e ){ }
	}
	
	public void testConstructor2()
	{
		assertNotNull(new BinaryNumber("1000"));
	}
	
	public void testConstructor3()
	{
		assertNotNull(new BinaryNumber("0"));
	}
	
	public void testConstructor4()
	{
		assertNotNull(new BinaryNumber("1"));
	}
	
	public void testConstructor5()
	{
		String s = "001010111010001011011111101001010101001010101001010000010100101010101011110010101101010111010010110010011101010010101001001101010010100010001011010101011111110101110101010010010010101010010100100101001001001110010010100101001011111000000000000000000000000000000011111111111111111111111111110000101001010001011010010110100101001110100100110010100";
		assertNotNull(new BinaryNumber(s));
	}
	
	public void testConstructor6()
	{
		try
		{
			new BinaryNumber(null);
			fail();
		}
		catch(Exception e){}
	}
	
	public void testConstructor7()
	{
		try
		{
			new BinaryNumber("12345");
			fail();
		}
		catch(Exception e){}
	}
	
	public void testConstructor8()
	{
		try
		{
			new BinaryNumber("abcdEFGH");
			fail();
		}
		catch(Exception e){}
	}
	
	public void testConstructor9()
	{
		try
		{
			new BinaryNumber("");
			fail();
		}
		catch(Exception e){}
	}
	
	public void testNot1()
	{
		BinaryADT x = new BinaryNumber("00000000");
		BinaryADT y = x.not();
		assertNotNull(y);
		assertNotSame(x,y);
	}
	public void testNot2()
	{
		BinaryADT x = new BinaryNumber("00000000");
		BinaryADT y = x.not();
		assertEquals("11111111",y.toString());
	}
	
	public void testNot3()
	{
		BinaryADT x = new BinaryNumber("111");
		BinaryADT y = x.not();
		assertEquals("000",y.toString());
	}
	
	public void testNot4()
	{
		BinaryADT x = new BinaryNumber("10101100");
		BinaryADT y = x.not();
		assertEquals("01010011",y.toString());
	}
	
	public void testAnd1()
	{
		BinaryADT x = new BinaryNumber("111");
		BinaryADT y = new BinaryNumber("000");
		BinaryADT z = x.and(y);
		assertNotSame(x,z);
		assertNotSame(y,z);
		assertEquals("000",z.toString());
	}
	
	public void testAnd2()
	{
		BinaryADT x = new BinaryNumber("101");
		BinaryADT y = new BinaryNumber("111");
		BinaryADT z = x.and(y);
		assertEquals("101",z.toString());
	}
	
	public void testAnd3()
	{
		BinaryADT x = new BinaryNumber("1");
		BinaryADT y = new BinaryNumber("111");
		BinaryADT z = x.and(y);
		assertEquals("001",z.toString());
	}
	
	public void testAnd4()
	{
		BinaryADT x = new BinaryNumber("111");
		BinaryADT y = new BinaryNumber("1");
		BinaryADT z = x.and(y);
		assertEquals("001",z.toString());
	}
	
	public void testAnd5()
	{
		BinaryADT x = new BinaryNumber("101110110111");
		BinaryADT y = new BinaryNumber("0000000000000000000000000000000000000000001");
		BinaryADT z = x.and(y);
		assertEquals("0000000000000000000000000000000000000000001",z.toString());
	}
	
	public void testAnd6()
	{
		BinaryADT x = new BinaryNumber("101110110111");
		BinaryADT y = null;
		try
		{
			BinaryADT z = x.and(y);
			// Good catch
		}
		catch( NullPointerException e )
		{
			fail();
		}
	}
	
	public void testAnd7()
	{
		BinaryADT x = new BinaryNumber("0000000000000000000000000000000000000000001");
		BinaryADT y = new BinaryNumber("101110110111");
		BinaryADT z = x.and(y);
		assertEquals("0000000000000000000000000000000000000000001",z.toString());
	}
	
	public void testOr1()
	{
		BinaryADT x = new BinaryNumber("111");
		BinaryADT y = new BinaryNumber("000");
		BinaryADT z = x.or(y);
		assertNotSame(x,z);
		assertNotSame(y,z);
		assertEquals("111",z.toString());
	}
	
	public void testOr2()
	{
		BinaryADT x = new BinaryNumber("101");
		BinaryADT y = new BinaryNumber("111");
		BinaryADT z = x.or(y);
		assertEquals("111",z.toString());
	}
	
	public void testOr3()
	{
		BinaryADT x = new BinaryNumber("1");
		BinaryADT y = new BinaryNumber("001");
		BinaryADT z = x.or(y);
		assertEquals("001",z.toString());
	}
	
	public void testOr4()
	{
		BinaryADT x = new BinaryNumber("001");
		BinaryADT y = new BinaryNumber("1");
		BinaryADT z = x.or(y);
		assertEquals("001",z.toString());
	}
	
	public void testOr5()
	{
		BinaryADT x = new BinaryNumber("101110110111");
		BinaryADT y = new BinaryNumber("0000000000000000000000000000000000000000001");
		BinaryADT z = x.or(y);
		assertEquals("0000000000000000000000000000000101110110111",z.toString());
	}
	
	public void testOr6()
	{
		BinaryADT x = new BinaryNumber("101110110111");
		BinaryADT y = null;
		try
		{
			BinaryADT z = x.or(y);
		}
		catch( NullPointerException e )
		{
			fail();
		}
	}
	
	public void testOr7()
	{
		BinaryADT x = new BinaryNumber("0000000000000000000000000000000000000000001");
		BinaryADT y = new BinaryNumber("101110110111");
		BinaryADT z = x.or(y);
		assertEquals("0000000000000000000000000000000101110110111",z.toString());
	}
	
	public void testXor1()
	{
		BinaryADT x = new BinaryNumber("111");
		BinaryADT y = new BinaryNumber("010");
		BinaryADT z = x.xor(y);
		assertNotSame(x,z);
		assertNotSame(y,z);
		assertEquals("101",z.toString());
	}
	
	public void testXor2()
	{
		BinaryADT x = new BinaryNumber("101");
		BinaryADT y = new BinaryNumber("111");
		BinaryADT z = x.xor(y);
		assertEquals("010",z.toString());
	}
	
	public void testXor3()
	{
		BinaryADT x = new BinaryNumber("1");
		BinaryADT y = new BinaryNumber("001");
		BinaryADT z = x.xor(y);
		assertEquals("000",z.toString());
	}
	
	public void testXor4()
	{
		BinaryADT x = new BinaryNumber("001");
		BinaryADT y = new BinaryNumber("1");
		BinaryADT z = x.xor(y);
		assertEquals("000",z.toString());
	}
	
	public void testXor5()
	{
		BinaryADT x = new BinaryNumber("101110110111");
		BinaryADT y = new BinaryNumber("0000000000000000000000000000000000000000001");
		BinaryADT z = x.xor(y);
		assertEquals("0000000000000000000000000000000101110110110",z.toString());
	}
	
	public void testXor6()
	{
		BinaryADT x = new BinaryNumber("101110110111");
		BinaryADT y = null;
		try
		{
			BinaryADT z = x.xor(y);
		}
		catch( NullPointerException e )
		{
			fail();
		}
	}
	
	public void testXor7()
	{
		BinaryADT x = new BinaryNumber("0000000000000000000000000000000000000000001");
		BinaryADT y = new BinaryNumber("101110110111");
		BinaryADT z = x.xor(y);
		assertEquals("0000000000000000000000000000000101110110110",z.toString());
	}
	
	public void testEquals1()
	{
		BinaryADT x = new BinaryNumber("110110111");
		BinaryADT y = null;
		try
		{
			boolean z = x.equals(y);
		}
		catch( NullPointerException e )
		{
			fail();
		}
	}
	
	public void testEquals2()
	{
		BinaryADT x = new BinaryNumber("101110110111");
		BinaryADT y = new BinaryNumber("0000000000000000000000000000000000000000001");
		assertFalse(x.equals(y));
		assertFalse(y.equals(x));
	}
	
	public void testEquals3()
	{
		BinaryADT x = new BinaryNumber("101110110111");
		BinaryADT y = new BinaryNumber("101110110111");
		assertTrue(x.equals(y));
		assertTrue(y.equals(x));
	}
	
	// I'm getting tired of writing tests.  toString() has already been tested, so no more methods to write tests
	// for.  Let's do two more -- De Morgan's Laws  If these don't work we're really messed up somewhere.
	
	public void testDeMorgan1()
	{
		// These ones do need to be the same length, otherwise it pads zeros out front differently depending on
		// whether a not is taken before an and or or.
		BinaryADT x = new BinaryNumber("0000001010100010101110000001011");
		BinaryADT y = new BinaryNumber("1100100111110100011111100010101");
		assertTrue((x.or(y)).not().equals(x.not().and(y.not())));
	}
	
	public void testDeMorgan2()
	{
		BinaryADT x = new BinaryNumber("0000001010100010101110000001011");
		BinaryADT y = new BinaryNumber("1100100111110100011111100010101");
		assertTrue((x.and(y)).not().equals(x.not().or(y.not())));
	}
	
	
}